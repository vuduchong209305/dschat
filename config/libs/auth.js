const jwt      = require('jsonwebtoken')

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    
    const token = authHeader && authHeader.split(' ')[1]

    if(token == null) return res.json({
        status: false,
        message: 'Token not found'
    })

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if(err) return res.json({
            status: false,
            message: err.message || err
        })
        req.user = user
        next()
    })
}

module.exports = {
    authenticateToken
}
module.exports = {
    dateFormat: 'HH:mm:ss | DD-MM-YYYY',
    timeZone: 'Asia/Bangkok',
}
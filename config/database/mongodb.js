const mongoose = require('mongoose');
const options = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
};
mongoose.Promise = global.Promise
mongoose.connect(process.env.MONGODB_URI, options)
mongoose.set('useFindAndModify', false);
const db = mongoose.connection;
db.on('error', () => console.error('Can not connect to MongoDB'))
db.once('open', () => console.log('MongoDB is connected'))
'use strict';
const socket = io.connect(location.host);

$('#message_form').submit(function(e){
	var self = this;
	e.preventDefault()
	if($('#msg').val() != '') {
		socket.emit('Client_send_message', $('#msg').val());
    	self.reset();
	}

	// Gửi ảnh
	if(document.getElementById("file_image").files.length > 0) {
		var data = new FormData();
	    $.each($('#file_image')[0].files, function(i, file) {
	        data.append('file-'+i, file);
	    });
	    console.log('data' + data)

	    $('div.gallery').empty();

	    $.ajax({
	        url: '/send_media',
	        data: data,
	        dataType: 'json',
	        contentType: false,
	        processData: false,
	        crossDomain: true,
	        type: 'POST',
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

	            $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
	            console.log('jqXHR:');
	            console.log(jqXHR);
	            console.log('textStatus:');
	            console.log(textStatus);
	            console.log('errorThrown:');
	            console.log(errorThrown);
	        },
	        success: function(res){
	    		console.log(res)
	            if(res.status == true) {
	            	var html;
	            	res.path.map((i,v) => {
	            		if(i.file_type == 'image') {
	            			html = `<div class="d-flex justify-content-end mb-4">
										<div class="msg_cotainer_send lightgallery">
											<a href="${i.file_path}" target="_blank"><img src="${i.file_path}" alt="" /></a>
											<span class="msg_time_send">${res.time}</span>
										</div>
										<div class="img_cont_msg">
											<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
										</div>
									</div>`
	            		} else {
	            			html = `<div class="d-flex justify-content-end mb-4">
										<div class="msg_cotainer_send lightgallery">
											<a href="${i.file_path}" target="_blank">
												<video width="320" height="240" controls>
												  	<source src="${i.file_path}" type="video/mp4">
												</video>
											</a>
											<span class="msg_time_send">${res.time}</span>
										</div>
										<div class="img_cont_msg">
											<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
										</div>
									</div>`
							
	            		}
	            		$('.msg_card_body').append(html);
	            	})

					socket.emit('Client_send_path_media', res);
	            }
	            self.reset();
	        }
	    });
	}
	

    // Gửi file
    if(document.getElementById("file_input").files.length > 0) {
	    var data2 = new FormData();
	    $.each($('#file_input')[0].files, function(i, file) {
	        data2.append('file-'+i, file);
	    });
	    console.log('data2' + data2)

	    $.ajax({
	        url: '/send_file',
	        data: data2,
	        dataType: 'json',
	        contentType: false,
	        processData: false,
	        crossDomain: true,
	        type: 'POST',
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

	            $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
	            console.log('jqXHR:');
	            console.log(jqXHR);
	            console.log('textStatus:');
	            console.log(textStatus);
	            console.log('errorThrown:');
	            console.log(errorThrown);
	        },
	        success: function(res){
	        	if(res.status == true) {
		            var html;
		        	res.path.map((i,v) => {
		        		html = `<div class="d-flex justify-content-end mb-4">
										<div class="msg_cotainer_send lightgallery">
											<a href="${i}" target="_blank"><h6>${i}</h6></a>
											<span class="msg_time_send">${res.time}</span>
										</div>
										<div class="img_cont_msg">
											<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
										</div>
									</div>`
						$('.msg_card_body').append(html);
		        	})
					socket.emit('Client_send_path_file', res);
				}
				self.reset();
	        }
	    });
	}
})

socket.on('Server_send_broadcast_message', res => {
	var html = `<div class="d-flex justify-content-start mb-4">
					<div class="img_cont_msg">
						<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
					</div>
					<div class="msg_cotainer">
						${res.msg}
						<span class="msg_time">${res.time}</span>
					</div>
				</div>`
	$('.msg_card_body').append(html);
})

socket.on('Server_send_message', res => {
	var html = `<div class="d-flex justify-content-end mb-4">
					<div class="msg_cotainer_send">
						${res.msg}
						<span class="msg_time_send">${res.time}</span>
					</div>
					<div class="img_cont_msg">
						<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
					</div>
				</div>`
	$('.msg_card_body').append(html);
})

socket.on('Server_send_broadcast_path_media', res => {
	res = JSON.parse(res)
	console.log(res)
	var html;
	res.path.map((i,v) => {
		if(i.file_type == 'image') {
			html = `<div class="d-flex justify-content-start mb-4">
						<div class="img_cont_msg">
							<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
						</div>
						<div class="msg_cotainer lightgallery">
							<a href="${i.file_path}" target="_blank"><img src="${i.file_path}" alt="" /></a>
							<span class="msg_time">${res.time}</span>
						</div>
					</div>`
			
		} else {
			html = `<div class="d-flex justify-content-start mb-4">
						<div class="img_cont_msg">
							<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
						</div>
						<div class="msg_cotainer lightgallery">
							<video width="320" height="240" controls>
							  	<source src="${i.file_path}" type="video/mp4">
							</video>
							<span class="msg_time">${res.time}</span>
						</div>
					</div>`
		}

		$('.msg_card_body').append(html);
	})
})

socket.on('Server_send_broadcast_path_file', res => {
	res = JSON.parse(res)
	var html;
	res.path.map((i,v) => {
		html = `<div class="d-flex justify-content-start mb-4">
						<div class="img_cont_msg">
							<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
						</div>
						<div class="msg_cotainer lightgallery">
							<a href="${i}" target="_blank"><h6>${i}</h6></a>
							<span class="msg_time">${res.time}</span>
						</div>
					</div>`
		$('.msg_card_body').append(html);
	})
})
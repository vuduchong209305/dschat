'use strict';

const Room = require('../models/RoomModel')
const Chat = require('../models/ChatModel')

function createRoom(req, res, next){
	const roomName = req.body.roomName.trim()
    const member = req.body.member
    const key_member = req.body.key_member

	if(roomName != '' && key_member != '') {
		const data = {
            name: roomName,
            member: member.split(","),
            key_member: key_member
        }

        Room.create(data, function (err, room) {
            if (err) {
                res.json({
                    status: false,
                    message: err.message || err
                });
            } else {
                Object.assign(data, {_id: room._id});
                res.json({
                    status: true,
                    message: `Tạo phòng ${room.name} thành công`,
                    data : data
                });
            }
        });
	} else {
        res.json({
            status: false,
            message: `Room name && key member bắt buộc`
        });
    }
}

function getListRoom(req, res, next) {
    Room.find({}).exec((err, data) => {
        if (err) {
            res.json({
                status: false,
                message: err.message || err
            });
        } else {
            res.json({
                status: true,
                message: `Success`,
                data : data
            });
        }
    })
}

function getMessageByRoom(req, res, next) {
    const current_room = req.query.current_room
    if(current_room != null) {

        Chat.find({'room_id': current_room}, (err, chat) => {
            if (err) {
				return res.json({
					status: false,
					message: err.message || err
				});
			} else {
                res.json({
                    status: true,
                    message: `Success`,
                    data : chat
                });
            }
        })

    } else {
        res.json({
            status: false,
            message: `Thiếu tham số đầu vào`
        });
    }
}

module.exports = {
    createRoom,
    getListRoom,
    getMessageByRoom
}
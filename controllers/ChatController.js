'use strict';

const formidable = require('formidable')
const Chat     = require('../models/ChatModel')

function sendMessage(req, res, next) {
    const room_id = req.body.room_id
    const user_id = req.body.user_id
    const newMessage = req.body.newMessage.toString().trim()

    if(room_id != null && user_id != null & newMessage != null) {
        const data = {
            room_id: room_id,
            user_id: user_id,
            content: newMessage
        }

        Chat.create(data, function (err, room) {
            if (err) {
                res.json({
                    status: false,
                    message: err.message || err
                });
            } else {
                res.json({
                    status: true,
                    message: `Gửi tin thành công`,
                    data : data
                });
            }
        });
    } else {
        res.json({
            status: false,
            message: `Thiếu tham số đầu vào`
        });
    }
}

function sendMedia(req, res, next) {
    const room_id = req.body.room_id
    const user_id = req.body.user_id
    console.log(req.body)
    const form = new formidable.IncomingForm();
    form.maxFileSize = 5000 * 5000 * 5000;
    form.maxFieldsSize = 5000 * 5000 * 5000;

    form.parse(req);

    var fileInfo = [];

    form.on('fileBegin', function (name, file){
        var file_name = Date.now() + '_' + file.name;
        file.path = "./public/uploads/" + file_name;
        fileInfo.push({
            file_path: "/uploads/" + file_name,
            file_type: file.type.includes('video') ? 'video' : 'image'
        });
    });

    form.on('end', (name, file) => {
        const data = {
            room_id: room_id,
            user_id: user_id,
            content: fileInfo
        }

        Chat.create(data, function (err, room) {
            if (err) {
                res.json({
                    status: false,
                    message: err.message || err
                });
            } else {
                res.json({
                    status: true,
                    message: `Gửi tin thành công`,
                    data : data,
                    type: 2,
                    u: 1,
                    time : timezone
                });
            }
        });

        
    });
}

module.exports = {
    sendMessage,
    sendMedia
}
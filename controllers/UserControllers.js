'use strict';
const User     = require('../models/UserModel')
const bcrypt   = require('bcryptjs')
const jwt      = require('jsonwebtoken')
const _		   = require('lodash')

function register(req, res, next) {

	const email = req.body.email.toString().trim()
	const password = req.body.password.toString().trim()

	if (email != '' && password != '') {
		bcrypt.genSalt(10, (err, salt) => {
			bcrypt.hash(password, salt, (err, hash) => {
				var userData = {
					email: email,
					password: hash
				}

				if(err) throw err
				User.create(userData, function (err, user) {
					if (err) {
						res.json({
							status: false,
							message: err.message || err
						});
					} else {
						res.json({
							status: true,
							message: `Đăng ký ${user.email} thành công`
						});
					}
				});
			})
		})
	}
}

function login(req, res, next) {

	const email = req.body.email.toString().trim().toLowerCase()
	const password = req.body.password.toString().trim()

	if(email != '' && password != '') {

		User.findOne({'email': email}, (err, row) => {
			if (err) {
				return res.json({
					status: false,
					message: err.message || err
				});
			}

			if (!_.size(row)) {
				return res.json({
					status: false,
					message: 'Tài khoản không tồn tại',
				});
			}

			if (!bcrypt.compareSync(req.body.password, String(row.password))) {
				return res.json({
					status: false,
					message: 'Mật khẩu không chính xác. Vui lòng thử lại.'
				});
			}

			const token = jwt.sign({ id: row.id }, process.env.ACCESS_TOKEN_SECRET || 'dschat');

			res.json({
				status: true,
				token: token,
				_id: row.id,
				email: row.email,
				message: 'Đăng nhập thành công'
			});
		})
	} else {
		res.json({
			status: false,
			message: 'Email & Password require'
		});
	}
}

function getUserListAddRoom(req, res, next) {
	const this_user = req.query.this_user
    User.find({'_id': {$ne: this_user}}).exec((err, data) => {
        if (err) {
            res.json({
                status: false,
                message: err.message || err
            });
        } else {
            res.json({
                status: true,
                message: `Success`,
                data : data
            });
        }
    })
}

module.exports = {
	register,
	login,
	getUserListAddRoom
}
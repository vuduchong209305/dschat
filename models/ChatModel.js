'use strict';

const config = require('../config');
const moment = require('moment-timezone');
moment.tz.setDefault(config.dateFormat);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ChatSchema = new Schema({
	user_id: {type: String, required: true},
	room_id: {type: String, required: true},
	type: {type: String, default: ''},
	content: {type: Array, default: '', required: true},
	time: {type: Number}
})

ChatSchema.pre('save', function (next) {
    if (!this.time)
        this.time = moment().unix();
    next();
});

module.exports = mongoose.model('Chat', ChatSchema);
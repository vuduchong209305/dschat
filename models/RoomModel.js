'use strict';

const config = require('../config');
const moment = require('moment-timezone');
moment.tz.setDefault(config.dateFormat);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var roomSchema = new Schema({
    name: { type: String, required: true, trim: true, unique: true },
    key_member: { type: String, require: true, trim: true},
    member: { type: Object, required: true, default: []},
    last_message: { type: Object },
    avatar: { type: String },
});

module.exports = mongoose.model('Room', roomSchema);
'use strict';

const config = require('../config');
const moment = require('moment-timezone');
moment.tz.setDefault(config.dateFormat);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
    email: { type: String, required: true, trim: true, unique: true },
    password: { type: String, required: true, trim: true },
});

module.exports = mongoose.model('User', userSchema);
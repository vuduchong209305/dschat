'use strict';
var express = require('express');
var router = express.Router();
const formidable = require('formidable')
var moment = require('moment-timezone');
const path = require('path');
const fs = require('fs');

router.get('/', (req, res, next) => {
	const directoryPath = path.join(__dirname, './../public/uploads');
	var arr_file = []
	fs.readdir(directoryPath, function (err, files) {
	    if (err) {return console.log('Unable to scan directory: ' + err)} 

	    res.render('index', { title: 'Express', files: files });
	});
});

router.post('/send_media', (req, res, next) => {
	if (req.url == "/send_media" && req.method.toLowerCase() == "post") {
		
		const form = new formidable.IncomingForm();
		form.maxFileSize = 5000 * 5000 * 5000;
		form.maxFieldsSize = 5000 * 5000 * 5000;

	    form.parse(req);

	    var fileInfo = [];

	    form.on('fileBegin', function (name, file){
			console.log(file.type)
	    	var file_name = Date.now() + '_' + file.name;
	        file.path = "./public/uploads/" + file_name;
	        fileInfo.push({
	        	file_path: "/uploads/" + file_name,
	        	file_type: file.type.includes('video') ? 'video' : 'image'
	        });
	    });

	    form.on('end', (name, file) => {
	        res.setHeader('Content-Type', 'application/json');
	        res.setHeader("Access-Control-Allow-Origin", "*");
	        res.setHeader("Access-Control-Allow-Credentials", false);
	        res.setHeader("Access-Control-Allow-Methods", "GET, POST,PUT");
		    res.end(JSON.stringify({ 
		    	status: true,
				path: fileInfo,
				type: 2,
				u: 1,
	    		time : timezone
		    }));
	    });
	}
})

router.post('/send_file', (req, res, next) => {
	if (req.url == "/send_file" && req.method.toLowerCase() == "post") {

		const form = new formidable.IncomingForm();
		form.maxFileSize = 5000 * 5000 * 5000;
		form.maxFieldsSize = 5000 * 5000 * 5000;

	    form.parse(req);

	    var file_path = [];
	    
	    form.on('fileBegin', function (name, file){
	    	
	    	var file_name = Date.now() + '_' + file.name;
	        file.path = "./public/uploads/" + file_name;
	        file_path.push("/uploads/" + file_name);
	    });

	    form.on('end', (name, file) => {
	        res.setHeader('Content-Type', 'application/json');
	        res.setHeader("Access-Control-Allow-Origin", "*");
	        res.setHeader("Access-Control-Allow-Credentials", false);
	        res.setHeader("Access-Control-Allow-Methods", "GET, POST,PUT");
		    res.end(JSON.stringify({ 
		    	status: true,
				path: file_path,
				type: 3,
				u: 1,
	    		time : timezone
		    }));
	    });
	}
})

router.post('/delete_file', (req, res, next) => {
	if (req.url == "/delete_file" && req.method.toLowerCase() == "post") {

        fs.unlink('./public/uploads/' + req.body.file, function (err) {
        	if (!err) {
        		res.json({
					status: true
				})
        	}
        });
	}
})

module.exports = router;

var express = require('express');
var router = express.Router();
const {authenticateToken} = require('./../config/libs/auth')

const {register, login, getUserListAddRoom} = require('../controllers/UserControllers')

router.get('/auth', (req, res, next) => {
	res.render('auth');
})

router.post('/register', register);
router.post('/login', login);
router.get('/getUserListAddRoom', authenticateToken, getUserListAddRoom);

module.exports = router;
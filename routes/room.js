var express = require('express');
var router = express.Router();
const {authenticateToken} = require('./../config/libs/auth')

const {createRoom, getListRoom, getMessageByRoom} = require('../controllers/RoomController')

router.post('/createRoom', authenticateToken, createRoom);
router.get('/getListRoom', authenticateToken, getListRoom);
router.get('/getMessageByRoom', authenticateToken, getMessageByRoom);

module.exports = router;
var express = require('express');
var router = express.Router();
const {authenticateToken} = require('./../config/libs/auth')

const {sendMessage, sendMedia} = require('../controllers/ChatController')

router.post('/sendMessage', authenticateToken, sendMessage);
router.post('/sendMedia', authenticateToken, sendMedia);

module.exports = router;
'use strict';
const createError  = require('http-errors');
const express      = require('express');
const path         = require('path');
const cookieParser = require('cookie-parser');
const logger       = require('morgan');
const bodyParser   = require('body-parser')
const passport     = require('passport');
const session      = require('express-session')
const flash        = require('connect-flash');
const cors         = require('cors')
const _            = require('lodash')
const app          = express();

require('dotenv').config()
require('./config/database')

app.use(express.json({limit: '50mb'}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json()); // support json encoded bodies

app.use(cookieParser('secret'));
app.use(flash());

app.use(session({
    secret : "secret",
    saveUninitialized: true,
    resave: true,
    cookie: { maxAge: 60000 }
}))

// Passport config
require('./config/passport')(passport)
// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use("/uploads", express.static(__dirname + '/uploads'));

app.use('/', require('./routes/index'));
app.use('/user', require('./routes/users'));
app.use('/room', require('./routes/room'));
app.use('/chat', require('./routes/chat'));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

const debug  = require('debug')('chat:server');
const http   = require('http');
const fs     = require('fs');
const moment = require('moment-timezone');
/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '8080');
app.set('port', port);

app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, function(){
    console.log("Listening port : " + port);
});

server.on('error', onError);
server.on('listening', onListening);

const timezone = moment(Date.now()).tz("Asia/Ho_Chi_Minh").format("HH:mm:ss DD-MM-YYYY")
global.timezone = timezone

const io = require('socket.io')(server);

var list_user = []
io.on('connection', socket => {

    console.log(`Connected : ${socket.id}`);

    // Server nhận tin nhắn và gửi trả lại user
    socket.on('Client_send_message', data => {
        console.log(data)
        if(data.current_room != null && data.newMessage != null) {
            socket.emit("Server_send_message", {
                msg: data.newMessage,
                type: 1,
                u:1,
                time: timezone
            });
            
            socket.broadcast.emit("Server_send_broadcast_message", {
                msg: data.newMessage,
                type: 1,
                u: 2,
                time: timezone
            });
        }
    })

    // Server nhận path media và gửi trả lại user
    socket.on('Client_send_path_media', data => {
        if(data != '') {
            Object.assign(data, {u: 2});
            socket.broadcast.emit("Server_send_broadcast_path_media", data);
        }
    })

    // Server nhận path file và gửi trả lại user
    socket.on('Client_send_path_file', data => {
        if(data != '') {
            Object.assign(data, {u: 2});
            socket.broadcast.emit("Server_send_broadcast_path_file", data);
        }
    })

    // Server trả list user đang online
    socket.on('Client_send_user_login', user => {
        if(typeof user != 'undefined' && user != '') {

            if(list_user.length == 0) {
                list_user.push(user)
            } else {
                if(list_user != null && !list_user.find(u => u.email == user.email)) {
                    list_user.push(user)
                }
            }
            
            io.sockets.emit('Server_send_list_user_online', {list_user: list_user, user_total: list_user.length})
        }
    })

    // Server check user logout & remove list
    socket.on('Client_logout', user => {
        list_user = _.reject(list_user, el => { 
            return el.id == user.id; 
        });
        io.sockets.emit('Server_send_list_user_online', {list_user: list_user, user_total: list_user.length})
    })
    
    socket.on('Client_check_loggedin', email => {
        if(list_user.length > 0) {
            var isCheck = list_user.filter(obj=>obj.email == email);
            if(isCheck.length > 0) {
                socket.emit('Server_check_loggedin', {status: false, message: 'Tài khoản này đang đăng nhập'})
            }
        }
    })

    io.sockets.emit('Server_send_list_user_online', {list_user: list_user, user_total: list_user.length})
    
    socket.on('disconnect', () => {
        console.log(`${socket.id} disconnected`);
    })
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}
